import { Component, OnInit } from '@angular/core';
import { Potato } from '../../../potatoes/potato.model';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  potatoSelected: Potato;

  constructor() {
    this.potatoSelected = null;
  }

  ngOnInit() {
  }

  /**
   * Appelé lorsque le component potato-list émet une nouvelle patate selectionnée.
   * @param $event la patate.
   */
  handlePotatoSelected($event) {
    if (!isNullOrUndefined(this.potatoSelected) && this.potatoSelected.name === $event.name) {
      this.potatoSelected = null;
    } else {
      this.potatoSelected = $event;
    }
  }

}
