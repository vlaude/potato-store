import { Potato } from './potato.model';
import * as PotatoActions from './potato.actions';
import { ADD_POTATO, REMOVE_POTATO } from './potato.actions';

const initialState: Potato[] = [
  { name: 'Super patate', image: 'patate_1.jpg' },
  { name: 'Patate rouge', image: 'patate_2.png' },
  { name: 'Belle patate', image: 'patate_3.jpg' },
  { name: 'La douce', image: 'patate_4.jpg' },
  { name: 'La croquante', image: 'patate_5.jpg' }
];

export function reducer(state: Potato[] = initialState, action: PotatoActions.Actions) {

  switch (action.type) {
    // Ajout d'une patate
    case ADD_POTATO :
      return [...state, action.payload];
    // Supression d'une patate
    case REMOVE_POTATO :
      const index = state.indexOf(action.payload);
      state.splice(index, 1);
      return state;

    default:
      return state;
  }

}
