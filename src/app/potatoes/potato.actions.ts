import { Action } from '@ngrx/store';
import { Potato } from './potato.model';

export const ADD_POTATO = '[Potato] Add';
export const REMOVE_POTATO = '[Potato] Remove';

export class AddPotato implements Action {
  readonly  type: string = ADD_POTATO;

  constructor(public payload: Potato) {}
}

export class RemovePotato implements Action {
  readonly type = REMOVE_POTATO;

  constructor(public payload: Potato) {}
}

export type Actions = AddPotato | RemovePotato;
