import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { Potato } from '../../potato.model';
import { AppState } from '../../../app.state';
import { Store } from '@ngrx/store';
import * as PotatoActions from '../../potato.actions';

@Component({
  selector: 'app-potato-list',
  templateUrl: './potato-list.component.html',
  styleUrls: ['./potato-list.component.css']
})
export class PotatoListComponent implements OnInit {
  potatoes: Observable<Potato[]>;
  @Output() potatoSelected = new EventEmitter<Potato>();

  constructor(private store: Store<AppState>) {
    this.potatoes = store.select('potatoes');
  }

  ngOnInit() {
  }

  /**
   * Emet la patate selectionnée.
   * @param event la patate.
   */
  emitSelectedPotato(event: Potato) {
    this.potatoSelected.emit(event);
  }

  delPotato(potato) {
    this.store.dispatch(new PotatoActions.RemovePotato(potato));
  }

}
