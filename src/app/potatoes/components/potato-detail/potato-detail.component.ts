import { Component, Input, OnInit } from '@angular/core';
import { Potato } from '../../potato.model';

@Component({
  selector: 'app-potato-detail',
  templateUrl: './potato-detail.component.html',
  styleUrls: ['./potato-detail.component.css']
})
export class PotatoDetailComponent implements OnInit {
  @Input() potato: Potato;

  constructor() { }

  ngOnInit() {
  }

}
