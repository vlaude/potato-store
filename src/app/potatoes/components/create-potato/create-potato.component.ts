import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AppState } from '../../../app.state';
import { Store } from '@ngrx/store';
import { AddPotato } from '../../potato.actions';

@Component({
  selector: 'app-create-potato',
  templateUrl: './create-potato.component.html',
  styleUrls: ['./create-potato.component.css']
})
export class CreatePotatoComponent implements OnInit {
  potatoForm = this.fb.group({
    name: ['', Validators.required]
  });

  constructor(
    private store: Store<AppState>,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    if (this.potatoForm.invalid) {
      return;
    }
    this.store.dispatch(new AddPotato(this.potatoForm.value));
    this.potatoForm.reset();
  }

}
