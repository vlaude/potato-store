import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePotatoComponent } from './create-potato.component';

describe('CreatePotatoComponent', () => {
  let component: CreatePotatoComponent;
  let fixture: ComponentFixture<CreatePotatoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePotatoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePotatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
