import { Potato } from './potatoes/potato.model';

export interface AppState {
  readonly potatoes: Potato[];
}
