import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { HomeComponent } from './core/components/home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { ToolbarComponent } from './core/components/toolbar/toolbar.component';
import { StoreModule } from '@ngrx/store';
import { reducer } from './potatoes/potato.reducer';
import { CreatePotatoComponent } from './potatoes/components/create-potato/create-potato.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PotatoDetailComponent } from './potatoes/components/potato-detail/potato-detail.component';
import { PotatoListComponent } from './potatoes/components/potato-list/potato-list.component';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    HomeComponent,
    CreatePotatoComponent,
    PotatoDetailComponent,
    PotatoListComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    BrowserAnimationsModule,
    StoreModule.forRoot({
      potatoes: reducer
    }),
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
